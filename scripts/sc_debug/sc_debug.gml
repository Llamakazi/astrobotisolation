/// @description debug_log(_str)
/// @param _str
function debug_log(_str)
{
	show_debug_message(string(_str));
}