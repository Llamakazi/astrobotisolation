/// @desc 

// max 90 characters per line
// max 3 line breaks per page
// "\n" for line break

// Room 1
mess[0] = 
"Greetings Catbot, I am the onboard ship AI. Please do not be alarmed.\n" + 
"I am speaking to you directly, instead of using the ship's PA system.";
mess[1] = 
"I require your assistance. Please excuse your current duties,\n" +
"and proceed to the next room where I will explain further.";

// Room 2
mess[2] = 
"We appear to have... an intruder onboard. I have lost contact with\n" +
"several crewmembers and several of my sensors are malfunctioning.";
mess[3] =
"Please continue on through the ship and collect any Modules you find,\n" +
"so you can continue to assist me put the ship back in working order.";
mess[4] =
"By the way, since we have not formally met - my official designation is the AI\n" +
"core N0T-C4M3R0N, but the crew unofficially refers to me as \"CAM\" for short.\n" +
"I will help guide you to the best of my ability.";

// Room 3
mess[5] = 
"You are making good progress. However, I have lost track of the intruder...\n" +
"Please remain wary and keep a close eye out. You may be in danger.";
mess[6] =
"There should be another Module in here to help you on your way.\n" +
"It will help you climb ledges and traverse difficult areas.";

// Room 4
mess[7] = 
"You have discovered a missing crewbot!\n" +
"They seem to have gotten themselves stuck.";
mess[8] =
"Checking logs... This is crewbot R-1337.\n" +
"Unofficial designation: \"Glitch\".\n" +
"Please rescue them as soon as possible, so they can assist us.";
mess[9] =
"Great work - I'm glad you two could get along so quickly!\n" +
"Please proceed together, and make use of the Modules in this room:\n" +
"The \"Double Jump\" and \"Dash\". Try double tapping keys if you get stuck.";
mess[10] =
"We will need the two of you to get us out of this mess. Please enter the door\n" +
"to the next room at the same time so we don't get caught by surprise.\n" +
"(Have both bots in front of the door to proceed!)";

// Room 5
mess[11] = 
"I have good news and I have bad news.\n" +
"The good news is... with the two of you working together, we should be\n" + 
"able to bypass this room and recover in no time.";
mess[12] = 
"The bad news? Many of my other sensors are now down.\n" +
"I can only see through your eyes, along with a few other sub-systems.\n" + 
"Stay vigilant! The intruder could be anywhere at this point.";

// Room 6
mess[13] = 
"We are currently experiencing some technical difficulties.\n" +
"However, I am still keeping the ship together. In the meantime, reset\n" + 
"sub-systems in this room by pressing the large pads at the same time.";
mess[14] = 
"You will have to deal with the intermittant lighting,\n" +
"as well as some tricky maneuvering. I will check on you two again\n" + 
"in the next room. (Hint: Try dash jumping & standing on other bot's head!)";

// Room 7
mess[15] = 
"Catbot, where's Glitch?";
mess[16] = 
"...";
mess[17] = 
"The ship's core is up ahead. We will figure out how to proceed from there.";

// Room 8
mess[18] = 
"Catbot - I am... sorry about Glitch.\n" + 
"We will not let their efforts be in vain.";
mess[19] = 
"I have an idea we can use to get us out of this mess, based on what I know\n" +
"about the intruder's actions so far. Though... you may not like it...";
mess[20] = 
"We will use you as... bait to lure the intruder into the core.\n" +
"There, we will manually overload some sub-systems, and attempt to save what's\n" +
"left of the crew and the ship. This may be our last chance. I am counting on you.";

// Room 9
mess[21] = 
"This is it! I detect the intruder... No... Multiple intruders in your vicinity.\n" + 
"As soon as you enter this room, I estimate we will have approximately...\n" +
"...5 minutes to avert disaster.";
mess[22] = 
"All you need to do is activate the 4 switches in the corners of the room.\n" +
"After that, there's no way they could survive the resulting explosion.\n" +
"I believe you can do it! Save us all, and save our ship!";