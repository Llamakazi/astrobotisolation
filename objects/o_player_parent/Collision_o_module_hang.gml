/// @description Enable Ledge Hang
if (modules.hang == false) {
	modules.hang = true;

	with (other) {
		instance_destroy();	
	}
}