/// @description Move to the next level
if (!instance_exists(o_text)) {
	var _other_player = this_player_num == 0 ? o_player_2 : o_player_1;
	
	if (!instance_exists(_other_player)) {
		if (place_meeting(x, y, o_door_open)) {
			y = yprevious;
			state = PlayerState.door;
			if (!audio_is_playing(a_exit)) {
				audio_play_sound(a_exit, 4, false);
			}
		}
	} else {
		with (_other_player) {
			var _this_player = this_player_num == 0 ? o_player_2 : o_player_1;
		
			if (place_meeting(x, y, o_door_open)) {
				y = yprevious;
				state = PlayerState.door;
				if (!audio_is_playing(a_exit)) {
					audio_play_sound(a_exit, 4, false);
				}
			
				with (_this_player) {
					y = yprevious;
					state = PlayerState.door;
				}
			}
		}
	}
}