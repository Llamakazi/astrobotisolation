/// @description Enable Jump
if (modules.jump == false) {
	modules.jump = true;

	with (other) {
		instance_destroy();
	}
}