/// @description Init shared Transition params
/// @param current_frame Starting frame of animation
/// @param max_frames Number of frames the Transition will animate (modify to change speed)

persistent = true; // when changing room keep this object alive

// Note: Set room_goto() IMMEDIATELY after creating this Transition object