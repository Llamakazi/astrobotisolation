/// @description Send state update if pushed

if (is_pushed) {
	is_pushed = false;
}
