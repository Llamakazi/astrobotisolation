/// @description Initialize the particle
image_speed = random_range(animation_speed_min, animation_speed_max);
speed = random_range(movement_speed_min, movement_speed_max);
direction = random(360);
gravity = initial_gravity;