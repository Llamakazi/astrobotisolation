/// @description Turn self On/Off Intermittent Interval

is_toggled = !is_toggled;

alarm[1] = intermittent_interval;