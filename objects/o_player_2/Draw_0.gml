/// @description Inherit parent, then draw Tutorial sprite if need be
event_inherited();

var _trigger_tutorial = instance_place(x, y, o_trigger_tutorial_p2);
if (_trigger_tutorial != noone) {
	if (_trigger_tutorial.sprite_tutorial_image_index == 4 && !modules.jump) {
		// Don't draw tutorial for jumping unless they have the proper Module
	} else {
		var _icon = _trigger_tutorial.sprite_tutorial_image_index;
		draw_sprite(
			_trigger_tutorial.sprite_tutorial_index, 
			_icon, 
			x, 
			y - 24
		);
	}
}