/// @description Stage 6 Init
trigger_event = false;
pads = [
	inst_6F8C6FF4, inst_3644AAA1
];
pads_length = 2;
toggles = [
	inst_51F4EE41, inst_1DB148D6, 
	inst_2E663887, inst_2FD1CF79, 
	inst_428BCA5,  inst_2C8D8633,
	inst_77D38DAB, inst_1716634A,
	inst_49C2F4EF, inst_67B88503
];
toggles_length = 10;

lighting = instance_create_layer(112, 16, "Lighting", o_lighting_fade);
lighting.image_xscale = 31;
lighting.image_yscale = 24;
lighting.image_alpha = lighting.alpha_min;