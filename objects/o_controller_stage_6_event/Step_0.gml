/// @desc Trigger the EVENT
var _i;
for (_i = 0; _i < pads_length; _i += 1) {
	var _target = instance_find(pads[_i], 0);
	if (pads[0].is_toggled && pads[1].is_toggled) {
		trigger_event = true;
		pads_length = 0;
	}
}

if (trigger_event) {
	for (var _i = 0; _i < toggles_length; _i++) {
		var _target = instance_find(toggles[_i], 0);
		if (_target != noone) {
			_target.is_toggled = !_target.is_toggled;
		}
	}
	
	o_lighting_fade.is_frozen = true;
	o_lighting_fade.alarm[0] = 180;
	o_eye_small.is_toggled = true;
	o_eye_small.destroy_alarm = 179;
	o_eye_small.depth = 0;
	instance_destroy(o_player_2);
	audio_stop_sound(a_two_robots);
	
	trigger_event = false;
}