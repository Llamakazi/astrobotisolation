/// @desc Handle Events and count
var _i;
for (_i = 0; _i < toggles_length; _i += 1) {
	var _target = instance_find(toggles[_i], 0);
	if (!toggles[0].is_toggled && 
		!toggles[1].is_toggled && 
		!toggles[2].is_toggled && 
		!toggles[3].is_toggled) {
		trigger_event = true;
	}
}

if (trigger_event) {
	alarm[0] = 180;
	instance_destroy(o_eye_all);
	instance_create_layer(352, 208, "Effects", o_explosion);
	toggles_length = 0;
	is_timer_active = false;
	trigger_event = false;
}

if (is_toggled && !is_toggled_previous) {
	//sprite_index = initial_sprite_index;
	is_toggled_previous = is_toggled;
	is_timer_active = true;
} else if (!is_toggled && is_toggled_previous) {
	// Cannot be turned off
	//sprite_index = -1;
	//is_toggled_previous = is_toggled;
}

if (is_timer_active) {
	timer--;
}

// Game Over
if (timer <= 0) {
	room_goto(r_game_over);
}