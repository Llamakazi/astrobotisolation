/// @description Init Timer
trigger_event = false;
toggles = [
	inst_50C01A10, inst_4AEE8965, 
	inst_59D156E8, inst_29C1A481
];
toggles_length = 4;

/*lighting = instance_create_layer(112, 16, "Lighting", o_lighting_fade);
lighting.image_xscale = 31;
lighting.image_yscale = 24;
lighting.image_alpha = lighting.alpha_min;*/

timer = 18000; // 5 minutes
is_timer_active = false;

audio_play_sound(a_boss_loop, 0, true);