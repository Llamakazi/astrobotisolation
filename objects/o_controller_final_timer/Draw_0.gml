/// @description Draw timer
draw_set_font(f_primary_large);
draw_set_halign(fa_center);
var _timer_seconds = string(floor(timer / 60));
draw_text_shadow(room_width / 2 - 8, 20, _timer_seconds, c_white, c_black, 0.7);