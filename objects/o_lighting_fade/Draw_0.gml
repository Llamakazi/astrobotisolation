/// @description Draw the transitions
if (!is_frozen) {
	var _alpha;
	if (current_frame < max_frames) {
		if (alpha_direction > 0) {
			_alpha = 1 - EaseOutExpo(current_frame, alpha_min, alpha_max, max_frames);
		} else {
			_alpha = EaseInExpo(current_frame, alpha_min, alpha_max, max_frames);
		}
	
		image_alpha = _alpha;
	}
} else {
	image_alpha = 1;
}

draw_self();