/// @description 
if (!is_frozen) {
	current_frame++;

	if (current_frame >= max_frames) {
		current_frame = 0;
		alpha_direction *= -1;
	}
}