/// @description Toggle Sprite

if (is_toggled && !is_toggled_previous) {
	sprite_index = initial_sprite_index;
	is_toggled_previous = is_toggled;
	alarm[0] = destroy_alarm;
} else if (!is_toggled && is_toggled_previous) {
	//sprite_index = -1;
	//is_toggled_previous = is_toggled;
}