/// @description Draw GUI based on the room
var _text = [];
_text[0] = "Great Success!";
_text[1] = "You have successfully saved yourself and fellow remaining crewmates!";
_text[2] = "The crewmates who gave their lives to save the others will not be forgotten.";
_text[3] = "/salute";
_text[4] = "(Created for the Secret Santa Game Jam 2020 https://itch.io/jam/secret-santa)";
_text[5] = "Press ESC to exit the game";

draw_set_colour(c_black);
draw_set_alpha(0.33);
draw_rectangle(0, 0, room_width, room_height, false);
draw_set_alpha(1);

draw_set_halign(fa_center);
draw_set_font(f_primary_large);
draw_text_shadow(room_width / 2, 24, _text[0], c_white, c_black, 0.7);

draw_set_font(f_primary);
draw_text_shadow(room_width / 2, room_height / 2 - 12, _text[1], c_white, c_black, 0.7);
draw_text_shadow(room_width / 2, room_height / 2,      _text[2], c_white, c_black, 0.7);
draw_text_shadow(room_width / 2, room_height / 2 + 12, _text[3], c_white, c_black, 0.7);
draw_text_shadow(room_width / 2, room_height / 2 + 24, _text[4], c_white, c_black, 0.7);

draw_text_shadow(room_width / 2, room_height - 48,     _text[5], c_white, c_black, 0.7);