/// @description Draw GUI based on the room
var _text = [];
_text[0] = "Astrobot: Isolation";
_text[1] = "The Year is 30XX";
_text[2] = "You are R-0451 (Unofficial designation: \"Catbot\") assigned to the SCS Eureka space junker.";
_text[3] = "While performing your routine duties, the ship's onboard AI makes a strange request of you.";
_text[4] = "This is where your story begins...";
_text[5] = "Press space to play";

draw_set_colour(c_black);
draw_set_alpha(0.33);
draw_rectangle(0, 0, room_width, room_height, false);
draw_set_alpha(1);

draw_set_halign(fa_center);
draw_set_font(f_primary_large);
draw_text_shadow(room_width / 2, 24, _text[0], c_white, c_black, 0.7);

draw_set_font(f_primary);
draw_text_shadow(room_width / 2, room_height / 2 - 24, _text[1], c_white, c_black, 0.7);
draw_text_shadow(room_width / 2, room_height / 2 - 12, _text[2], c_white, c_black, 0.7);
draw_text_shadow(room_width / 2, room_height / 2,      _text[3], c_white, c_black, 0.7);
draw_text_shadow(room_width / 2, room_height / 2 + 12, _text[4], c_white, c_black, 0.7);

draw_text_shadow(room_width / 2, room_height - 48,     _text[5], c_white, c_black, 0.7);