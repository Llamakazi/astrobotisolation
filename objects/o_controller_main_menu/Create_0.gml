/// @description Init Player vars on creation

PlayerModules = function() constructor
{
	jump = false;
	hang = false;
	dash = false;
	double = false;
	blast = false;
}

global.player_1_modules = new PlayerModules();
global.player_2_modules = new PlayerModules();
