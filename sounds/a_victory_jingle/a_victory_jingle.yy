{
  "compression": 0,
  "volume": 0.2,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "a_victory_jingle.wav",
  "duration": 9.025839,
  "parent": {
    "name": "Music",
    "path": "folders/Sounds/Music.yy",
  },
  "resourceVersion": "1.0",
  "name": "a_victory_jingle",
  "tags": [],
  "resourceType": "GMSound",
}