{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 89,
  "bbox_top": 0,
  "bbox_bottom": 62,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 100,
  "height": 74,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"b6f1a794-be6b-429b-b1c8-990cff30c0a1","path":"sprites/s_background_eyes/s_background_eyes.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b6f1a794-be6b-429b-b1c8-990cff30c0a1","path":"sprites/s_background_eyes/s_background_eyes.yy",},"LayerId":{"name":"50f614f7-471d-4951-9c75-4edd5a1044d0","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"s_background_eyes","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","name":"b6f1a794-be6b-429b-b1c8-990cff30c0a1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c74ef424-7b6a-4fc7-b94a-fc4a3dbde30c","path":"sprites/s_background_eyes/s_background_eyes.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c74ef424-7b6a-4fc7-b94a-fc4a3dbde30c","path":"sprites/s_background_eyes/s_background_eyes.yy",},"LayerId":{"name":"50f614f7-471d-4951-9c75-4edd5a1044d0","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"s_background_eyes","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","name":"c74ef424-7b6a-4fc7-b94a-fc4a3dbde30c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"7cff76eb-bc82-40e7-b2f2-dc99ce493261","path":"sprites/s_background_eyes/s_background_eyes.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7cff76eb-bc82-40e7-b2f2-dc99ce493261","path":"sprites/s_background_eyes/s_background_eyes.yy",},"LayerId":{"name":"50f614f7-471d-4951-9c75-4edd5a1044d0","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"s_background_eyes","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","name":"7cff76eb-bc82-40e7-b2f2-dc99ce493261","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a21946f6-b97f-4744-b16a-2ca30f5c75f6","path":"sprites/s_background_eyes/s_background_eyes.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a21946f6-b97f-4744-b16a-2ca30f5c75f6","path":"sprites/s_background_eyes/s_background_eyes.yy",},"LayerId":{"name":"50f614f7-471d-4951-9c75-4edd5a1044d0","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"s_background_eyes","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","name":"a21946f6-b97f-4744-b16a-2ca30f5c75f6","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e2802964-901c-470f-848a-197fe50ca772","path":"sprites/s_background_eyes/s_background_eyes.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e2802964-901c-470f-848a-197fe50ca772","path":"sprites/s_background_eyes/s_background_eyes.yy",},"LayerId":{"name":"50f614f7-471d-4951-9c75-4edd5a1044d0","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"s_background_eyes","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","name":"e2802964-901c-470f-848a-197fe50ca772","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"225c20c2-0e07-4030-89d0-78b97b1b7892","path":"sprites/s_background_eyes/s_background_eyes.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"225c20c2-0e07-4030-89d0-78b97b1b7892","path":"sprites/s_background_eyes/s_background_eyes.yy",},"LayerId":{"name":"50f614f7-471d-4951-9c75-4edd5a1044d0","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"s_background_eyes","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","name":"225c20c2-0e07-4030-89d0-78b97b1b7892","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f499946e-7f24-4a20-8abe-6d542bd4a9d9","path":"sprites/s_background_eyes/s_background_eyes.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f499946e-7f24-4a20-8abe-6d542bd4a9d9","path":"sprites/s_background_eyes/s_background_eyes.yy",},"LayerId":{"name":"50f614f7-471d-4951-9c75-4edd5a1044d0","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"s_background_eyes","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","name":"f499946e-7f24-4a20-8abe-6d542bd4a9d9","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"aab73377-2966-406c-b2dc-74e0d4fe5f44","path":"sprites/s_background_eyes/s_background_eyes.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"aab73377-2966-406c-b2dc-74e0d4fe5f44","path":"sprites/s_background_eyes/s_background_eyes.yy",},"LayerId":{"name":"50f614f7-471d-4951-9c75-4edd5a1044d0","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"s_background_eyes","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","name":"aab73377-2966-406c-b2dc-74e0d4fe5f44","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"fc023503-c06c-4107-9890-a3930606e240","path":"sprites/s_background_eyes/s_background_eyes.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fc023503-c06c-4107-9890-a3930606e240","path":"sprites/s_background_eyes/s_background_eyes.yy",},"LayerId":{"name":"50f614f7-471d-4951-9c75-4edd5a1044d0","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"s_background_eyes","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","name":"fc023503-c06c-4107-9890-a3930606e240","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"baa8f58d-6172-415e-a5df-5b47f2926775","path":"sprites/s_background_eyes/s_background_eyes.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"baa8f58d-6172-415e-a5df-5b47f2926775","path":"sprites/s_background_eyes/s_background_eyes.yy",},"LayerId":{"name":"50f614f7-471d-4951-9c75-4edd5a1044d0","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"s_background_eyes","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","name":"baa8f58d-6172-415e-a5df-5b47f2926775","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"855ea31b-7a5c-4e65-a771-cba181670934","path":"sprites/s_background_eyes/s_background_eyes.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"855ea31b-7a5c-4e65-a771-cba181670934","path":"sprites/s_background_eyes/s_background_eyes.yy",},"LayerId":{"name":"50f614f7-471d-4951-9c75-4edd5a1044d0","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"s_background_eyes","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","name":"855ea31b-7a5c-4e65-a771-cba181670934","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d6b2c7ea-20b9-4627-a7d4-f6a530e66e74","path":"sprites/s_background_eyes/s_background_eyes.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d6b2c7ea-20b9-4627-a7d4-f6a530e66e74","path":"sprites/s_background_eyes/s_background_eyes.yy",},"LayerId":{"name":"50f614f7-471d-4951-9c75-4edd5a1044d0","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"s_background_eyes","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","name":"d6b2c7ea-20b9-4627-a7d4-f6a530e66e74","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a9917fa2-8419-4790-b292-33d091a349d4","path":"sprites/s_background_eyes/s_background_eyes.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a9917fa2-8419-4790-b292-33d091a349d4","path":"sprites/s_background_eyes/s_background_eyes.yy",},"LayerId":{"name":"50f614f7-471d-4951-9c75-4edd5a1044d0","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"s_background_eyes","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","name":"a9917fa2-8419-4790-b292-33d091a349d4","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"39a4ca15-b771-4ede-b8be-5416facb9a56","path":"sprites/s_background_eyes/s_background_eyes.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"39a4ca15-b771-4ede-b8be-5416facb9a56","path":"sprites/s_background_eyes/s_background_eyes.yy",},"LayerId":{"name":"50f614f7-471d-4951-9c75-4edd5a1044d0","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"s_background_eyes","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","name":"39a4ca15-b771-4ede-b8be-5416facb9a56","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b044bab9-a36a-46d9-9c14-f978a83d1974","path":"sprites/s_background_eyes/s_background_eyes.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b044bab9-a36a-46d9-9c14-f978a83d1974","path":"sprites/s_background_eyes/s_background_eyes.yy",},"LayerId":{"name":"50f614f7-471d-4951-9c75-4edd5a1044d0","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"s_background_eyes","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","name":"b044bab9-a36a-46d9-9c14-f978a83d1974","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"9b7ba153-75e6-4c51-a418-9eb64fa34146","path":"sprites/s_background_eyes/s_background_eyes.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9b7ba153-75e6-4c51-a418-9eb64fa34146","path":"sprites/s_background_eyes/s_background_eyes.yy",},"LayerId":{"name":"50f614f7-471d-4951-9c75-4edd5a1044d0","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"s_background_eyes","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","name":"9b7ba153-75e6-4c51-a418-9eb64fa34146","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2192c81c-c755-41d8-91f5-77ecd1f7e401","path":"sprites/s_background_eyes/s_background_eyes.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2192c81c-c755-41d8-91f5-77ecd1f7e401","path":"sprites/s_background_eyes/s_background_eyes.yy",},"LayerId":{"name":"50f614f7-471d-4951-9c75-4edd5a1044d0","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"s_background_eyes","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","name":"2192c81c-c755-41d8-91f5-77ecd1f7e401","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"bf61fee3-7320-46f4-b572-b089e57a8833","path":"sprites/s_background_eyes/s_background_eyes.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"bf61fee3-7320-46f4-b572-b089e57a8833","path":"sprites/s_background_eyes/s_background_eyes.yy",},"LayerId":{"name":"50f614f7-471d-4951-9c75-4edd5a1044d0","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"s_background_eyes","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","name":"bf61fee3-7320-46f4-b572-b089e57a8833","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"s_background_eyes","path":"sprites/s_background_eyes/s_background_eyes.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 6.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 18.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"3208b41b-a54a-4261-b71a-d83d14d23b8b","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b6f1a794-be6b-429b-b1c8-990cff30c0a1","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"40efdad7-870b-44d4-868f-78658b2c92a4","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c74ef424-7b6a-4fc7-b94a-fc4a3dbde30c","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b9d498ba-e1b9-4581-b0ec-c98aa78d4f87","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7cff76eb-bc82-40e7-b2f2-dc99ce493261","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"30bc0cd7-7876-47c7-b7e1-8c53adf7c748","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a21946f6-b97f-4744-b16a-2ca30f5c75f6","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5c00ba64-246c-4bf0-8c49-ac29288f9414","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e2802964-901c-470f-848a-197fe50ca772","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"83e56b78-a799-47a7-9866-31e67f9e7176","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"225c20c2-0e07-4030-89d0-78b97b1b7892","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f263ef42-252b-435c-9edf-c3bbad78e2de","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f499946e-7f24-4a20-8abe-6d542bd4a9d9","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a43ef2c7-12e9-43fe-a077-8e6bee850fcb","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"aab73377-2966-406c-b2dc-74e0d4fe5f44","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"190b07d0-5844-4944-9da3-a3ff15e7bd1b","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fc023503-c06c-4107-9890-a3930606e240","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0e6f3d4c-0910-4b1e-9dea-3824e3b4efa9","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"baa8f58d-6172-415e-a5df-5b47f2926775","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8a692081-9c2a-4b85-8da3-eb0be87a3de3","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"855ea31b-7a5c-4e65-a771-cba181670934","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4b90fc23-10a6-4382-b575-53dfe5acf9f8","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d6b2c7ea-20b9-4627-a7d4-f6a530e66e74","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"776ae6e5-5d0a-400b-93ba-93f69592a0b4","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a9917fa2-8419-4790-b292-33d091a349d4","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8eeebf8b-8e03-4cec-adbb-2c3ccae1a123","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"39a4ca15-b771-4ede-b8be-5416facb9a56","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7eaf2f80-1801-40fd-a2dc-b9d50affb087","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b044bab9-a36a-46d9-9c14-f978a83d1974","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"43a369b7-0d62-41ce-a492-ff474628742a","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9b7ba153-75e6-4c51-a418-9eb64fa34146","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"df461f2f-f7c9-4e48-a7af-d599b321e868","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2192c81c-c755-41d8-91f5-77ecd1f7e401","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"00729a45-54a9-405c-82d2-314ea7d6f9b9","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"bf61fee3-7320-46f4-b572-b089e57a8833","path":"sprites/s_background_eyes/s_background_eyes.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 50,
    "yorigin": 37,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"s_background_eyes","path":"sprites/s_background_eyes/s_background_eyes.yy",},
    "resourceVersion": "1.3",
    "name": "s_background_eyes",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"50f614f7-471d-4951-9c75-4edd5a1044d0","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Backgrounds",
    "path": "folders/Sprites/Level/Backgrounds.yy",
  },
  "resourceVersion": "1.0",
  "name": "s_background_eyes",
  "tags": [],
  "resourceType": "GMSprite",
}